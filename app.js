/* global angular */
'use strict';

angular.module('app',[])
	.constant("serviceBaseUrl", "http://beta.maptasking.com/")
	.run(function (authFactory) {
		//DEBUG: si la autenticacion funciona veremos la respuesta del token endpoint en el console
		//TO DO para participantes: borrar esto antes de terminar el reto
		
		console.log("---BIENVENIDO AL RETO ANGULAR---");
		
		var loginData = {
			userName: "dev@nexsyscomputing.com",
			password: "retoAngular"
		}

		authFactory.login(loginData).then(
			function (response) {
				console.log("token response", response.data);
			},
			function (errorResponse) {
				console.log("error haciendo login", errorResponse);
			}
			);
	})
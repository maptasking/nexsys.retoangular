/* global angular */
'use strict';

angular.module('app')
    .factory('authFactory', ['$http', '$q', 'serviceBaseUrl', function ($http, $q, serviceBaseUrl) {
        return {
            login: function (loginData) {
                var deferred = $q.defer();
                var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;
                $http.post(serviceBaseUrl + 'token', data,
                    {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(
                        function (response) {
                            deferred.resolve(response);
                        },
                        function (errorResponse) {
                            deferred.reject(errorResponse);
                        });
                return deferred.promise;
            }
        }
    }])

